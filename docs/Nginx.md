# Commands

## Signal
>`nginx -s`

sends signals

- **stop** fast shutdown
- **quit** "graceful" shutdown executed under same user that started
- **reload** reloads config file

## Test

>`nginx -t`

tests config file


# Concepts

NGINX is a free, open-source, high-performance HTTP server and reverse proxy. NGINX is known for its high performance, stability, rich feature set, simple configuration, and low resource consumption.

## Reverse Proxy Server
**Normal proxy** servers act like middlemen for **clients** to communicate with the internet

```
+----------+    +-------+    +-----------+
|          |    |       |<-->|  Client   |
|  Server  |    |       |    +-----------+
|    OR    |<-->| Proxy |
| Internet |    |       |    +-----------+
|          |    |       |<-->|  Client   |
+----------+    +-------+    +-----------+
```

**Reverse proxy** servers act like middlemen for **servers**

```
+----------+    +-------+    +------------+
|  Server  |<-->|       |    |            |
+----------+    |Reverse|    |            |
                |       |<-->|  Internet  |
+----------+    | Proxy |    |            |
|  Server  |<-->|       |    |            |
+----------+    +-------+    +------------+

```
# Config File
server config file in /etc/nginx/nginx.conf. Server config file consist of key-value pairs called __directives__ and parameters. 

Top-level directives are called __contexts__

>directive(key) parameter(value)
>
>`worker_processes 1;`

>context
> * `events` connection processing
> * `http` http traffic
> * `mail` mail
> * `stream` tcp/udp
!!http context
```
http {
    server {
        listen 8080;
        root /data/www;

        location /first_app/ {
            root /data/app/one_dir;
        }
        location /second_app/ {
            proxy_pass 192.168.1.172;
        }
    } 
    
    server {
        listen 127.0.0.1:80;
        server_name example.com www.example.com
    }
    ...
}
```
**server** is for each virtual server

**listen** is the port nginx is listening

**location** is the URI from the request 

>Eg. www.gaming.com/dashboad/profile
>
>location would be ''__/dashboad/profile__''

**root** is the root directory nginx uses to serve

**server_name** can match request's HTTP header's __Host__ field 

> Eg. 
>
> www.gaming.com/dashboad/profile
>
> Host field would be **www.gaming.com**
>
>  gaming.com/dashboad/profile
>
> Host field would be **gaming.com**

**proxy-pass** passes request to proxied server and pass response back

## Root and Location and Alias
Directories

```
data/
├─ first_app/
│  ├─ server.js
|  ├─ images/
|     ├─ cute_dog.png
|
├─ second_app/
│  ├─ server.js
|  ├─ pictures/
|     ├─ cute_cat.png

```
nginx.conf

```conf
server {
    location /images/ {
        root /data/first_app;
    }

    location /images/ {
        alias /data/second_app/pictures/;
    }
}
```

>All traffic from example.com/images/ will be sent to
>
>__/data/first_app__''/images/''
>
>__root__/''location''


## But
>
>All traffic from example.com/pictures/ will be sent to
>
>''/data/second_app/pictures/''
>
>''alias''
