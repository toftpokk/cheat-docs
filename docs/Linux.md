# Problem Solving

## Terminal Showing ^[[A ^[[B
### Cause
Terminal uses shell rather than bash
### Solution
>`sudo chsh -s /bin/bash ${username}`

## Internet not working on Ubuntu system
### Solution
Set static ip address

>use netplan
>
> `sudo nano /etc/netplan/01-netcfg.yaml` or `/01-network-manager-all.yaml`

```yaml
network:
    version: 2
    Renderer: networkd
    ethernets:
       DEVICE_NAME:
          dhcp4: yes
          addresses: [IP/NETMASK]
          gateway4: GATEWAY
          nameservers:
             Addresses: [NAMESERVER, NAMESERVER]
```
> `sudo netplan try`
> to check yaml file
>
> `sudo netplan apply`
> to apply changes
# Special Files and environments


`/bin/` binaries folder for system files

`/etc/apt/sources.list` apt sources list

## apt vs apt-get
> ''apt'' is pleasant for end users

>''apt-get'' and ''apt-cache'' are low level commands


`apt install` installs package

`apt remove` removes package

`apt purge` removes package and configs

`apt update` pulls from repository

`apt upgrade` upgrades packages

`apt autoremove` removes unwanted packages

`apt search <string>` search for package

`apt show <string>` shows info about package

`apt list` list packages

# File Management


## chgrp
`chgrp <groupname> <foldername>`

changes folder group

## chmod
`chmod [options] <file or directory>`

 changes access permission of file
> -R recursive
>` chmod <octetoctetoctet> file`
>- 1 owner
>- 2 group
>- 3 other
>
> +0 none +1 execute +2 write +4 read
>
> `chmod <class><operator><modes>,... file`
>
> mode rwx - read write execute
>- u - user
>- g - group
>- o - other


## env
`env`

shows environmental variables

# User Management

## useradd
`useradd [options] <username>`

adds a new user
> -d [path] add home directory to /home/$username or path
>
> -M no home directory
>
> -u [id] sets user id
>
> -G [groupA,groupB,...] adds user to groups


## passwd
`passwd [username]`

sets password of current user or username

## groups
`groups [username]`

list groups of current user or username

## groupadd
`groupadd <groupname>`

adds new group

## getent
`getent group <group name>`

list all members of group name

## id
`id [username]`

gets ids and groups of current user or username

## Create home directory for new users

You will need to create the users directory manually. This requires three steps:
>>>
> 1. Create directory in compliance to /etc/passwd, usually there will be already a /home/login entry.
> 2. Copy initial files from /etc/skel
>
> 3. And finally set right permissions:
>       - mkdir /home/YOU
>       - cd /home/YOU
>       - cp -r /etc/skel/. .
>       - chown -R YOU.YOURGROUP .
>       - chmod -R go=u,go-w .
>       - chmod go= .

[[SysAdminDocs]]