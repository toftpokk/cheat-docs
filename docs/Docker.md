# Commands

## Run
>`docker run <image> [options]`

pulls image if not local and starts the image

- **d**  detached mode

- **p** [host-port]:[container-port] bind ports

- **e** [variable-name]=[value] sets environmental variables

- **name** [container-name] sets name for container

- **net** [network-name] sets container network

## Start
> `docker start <container name or id>`

starts container with name or id 
## Stop
>`docker stop <container name or id>`

starts container with name or id

## Remove
>`docker rm <container name or id>`

removes docker container

## List Processes
>`docker ps [options]`

list running processes

-a lists both running and not running

## Images
>`docker images`

list all local images

## Remove Image
>`docker rmi <image>`

removes docker image

## Logs
>`docker logs [container name or id]`

shows logs for container with name or id

## Execute
>`docker exec [options]`

executes command in container

-it < container id > /bin/bash

## Create Network
>`docker network create <network-name>`

creates a docker network

## List Network
>`docker network ls`

lists networks

## Build
>`docker build`


### Compose
>`docker-compose -f <yaml file> <up|down> [options]`

runs containers according to the yaml file and starts a new network

''up'' runs the container and creates a network

''down'' stops the container and removes the network

# Concepts
Docker runs only the application layer and uses the same kernel as the host, docker on windows will work differently.

Data in containers is not persistent

## Images vs Containers
containers are running (or stopped) images
images are just files

### Comparison
| image   | container       |
| ------- | --------------- |
|exe file |program          |
|iso file |operating system |

## Networks
Docker containers can talk using just the container name in the same network.
 Docker containers use the host's ports as specified in the -p option

## Docker Compose
''run'' containers according to the yaml file and ''creates'' a new network

## Docker File
builds new images to run docker containers according to a file named "Dockerfile"

''RUN'' runs commands inside a container
''COPY'' copies files from host to container
''CMD'' runs the entry command 

**there is only one CMD command in a docker file**

adjusting dockerfiles needs rebuilding

# Files
## Docker Compose
```yaml
version: '3'
  services:
    container-name:
      image: 'image-name'
      ports:
      - host:container
      environment:
      - ENVIR_VARIABLE=value
    container-name:
      ...

```
## Docker File
```Dockerfile
FROM base_image
RUN command values
RUN ...
COPY host_files container_dest
COPY ...
ENV ENV_VAR=value \
    ENV_2VAR=value2
    ...
CMD ["command", "value1" ... ]
```